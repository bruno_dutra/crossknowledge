<?php

class Router
{
  private $routes;
  
  public function __construct()
  {
    $this->routes = array();
    
    $this->setup();
  }
  
  protected function setup() {}
  
  // Try to match a given path into router rules
  public function match($path)
  {
    $info = array(
      'controller' => null,
      'action' => $this->default_action(),
      'params' => array()
    );
    
    $found = false;
    
    foreach ($this->routes as $route) {
      $matches = array();
      $data = array();
      $pattern = $route['match_pattern'];
      
      if (preg_match($pattern, $path, $matches)) {
        foreach ($route['names'] as $key => $name) {
          $data[$name] = $matches[$key + 1];
        }
        
        foreach (array('controller', 'action') as $value) {
          if ($route['options'][$value]) {
            $data[$value] = $route['options'][$value];
          }
        }
        
        $found = true;
        
        break;
      }
    }
    
    if (!$found) {
      throw new InvalidRouterException('Route not found');
    }
    
    foreach ($data as $key => $value) {
      if (in_array($key, array('controller', 'action'))) {
        $info[$key] = $value;
      } else {
        $info['params'][$key] = $value;
      }
    }
    
    return $info;
  }
  
  private function escape_pattern_char($char)
  {
    $to_escape = './\\()[]?*+';
    
    if (strpos($to_escape, $char) !== false) {
      $char = '\\' . $char;
    }
    
    return $char;
  }
  
  // Default action to be used
  public function default_action()
  {
    return 'index';
  }
  
  // Create a router rule
  public function map_connect($pattern, $options = array())
  {
    $options = array_merge($this->default_options(), $options);
    
    $extract_match = $this->create_match_pattern($pattern);
    
    $route = array();
    $route['pattern'] = $pattern;
    $route['match_pattern'] = $extract_match['pattern'];
    $route['names'] = $extract_match['names'];
    $route['name'] = $options['name'];
    $route['options'] = $options;
    
    $this->routes[] = $route;
  }
  
  // Evaluate a match pattern
  public function create_match_pattern($pattern)
  {
    $var_buffer = '';
    $cur = 0;
    $max = strlen($pattern);
    $state = 0;
    $match_pattern = '/^';
    $names = array();
    
    while ($cur < $max) {
      $char = $pattern[$cur];
      
      if ($state == 0) {
        if ($char == ':') {
          $var_buffer = '';
          $state = 1;
        } else {
          $match_pattern .= $this->escape_pattern_char($char);
        }
      } elseif ($state == 1) {
        $code = ord($char);
        
        if ($char == '_' || ($code > 96 && $code < 123)) {
          $var_buffer .= $char;
        } else {
          $match_pattern .= '([a-z0-9_-]+?)';
          $match_pattern .= $this->escape_pattern_char($char);
          $state = 0;
          $names[] = $var_buffer;
          $var_buffer = '';
        }
      }
      
      $cur++;
    }
    
    if ($state == 1) {
      $match_pattern .= '([a-z0-9_-]+?)';
      $names[] = $var_buffer;
    }
    
    $match_pattern .= '$/i';
    
    return array("pattern" => $match_pattern, "names" => $names);
  }

  public function __call($method, $args)
  {
    if (substr($method, 0, 4) == 'map_') {
      $name = substr($method, 4);
      
      $pattern = $args[0];
      
      if (count($args) > 1) {
        $options = $args[1];
      } else {
        $options = array();
      }
      
      $options['name'] = $name;
      
      return $this->map_connect($pattern, $options);
    }
    
    throw new InvalidRouterException('Method not found');
  }
  
  private function default_options()
  {
    return array(
      'controller' => null,
      'action' => null,
      'name' => null
    );
  }
}

class InvalidRouterException extends Exception {}
