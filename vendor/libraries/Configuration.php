<?php

class Configuration
{
  private $data;
  
  public function __construct()
  {
    $this->data = array();
  }
  
  public function __get($propertie)
  {
    return isset($this->data[$propertie]) ? $this->data[$propertie] : null;
  }
  
  public function __set($propertie, $value)
  {
    $this->data[$propertie] = $value;
  }
}
