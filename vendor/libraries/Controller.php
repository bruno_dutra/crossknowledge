<?php

abstract class Controller
{
  protected $_current_route;
  protected $_data;
  protected $_render;
  protected $_params;
  
  public final function __construct()
  {
    $this->_data = array();
    $this->_render = true;
    $this->_params = array_merge($_GET, $_POST);
  }
  
  public static function build_view_path($controller, $action)
  {
    return APP_VIEWS_PATH . '/' . str_replace('_', '/', strtolower($controller)) . '/' . $action . '.php';
  }
  
  protected function classname($lowercase = true)
  {
    $name = substr(get_class($this), 0, -strlen('Controller'));
    
    if ($lowercase) {
      $name = strtolower($name);
    }
    
    return $name;
  }
  
  protected function view_path($action, $controller = null)
  {
    if ($controller === null) {
      $controller = $this->classname();
    }
    
    $path = self::build_view_path($controller, $action);
    
    return $path;
  }
  
  // Execute main routine of an action
  public function execute($method, $args = array())
  {
    $view_path = $this->view_path($method);
    
    if (!method_exists($this, $method) && !file_exists($view_path)) {
      throw new Exception("Not found $method in " . $this->classname());
    }
    
    if (method_exists($this, $method)) {
      call_user_func_array(array($this, $method), $args);
    }
    
    if ($this->_render) {
      $this->render($method);
    }
  }
  
  // Do an execution based on a route
  public static function run($route) {
    $controller_name = ucfirst($route['controller']) . 'Controller';
    $method = strtolower($route['action']);
    
    $controller = new $controller_name();
    $controller->_current_route = $route;
    $controller->_params = array_merge($controller->_params, $route['params']);
    $controller->execute($method);
  }

  protected function param($name, $default = null)
  {
    return isset($this->_params[$name]) && !empty($this->_params[$name]) ? $this->_params[$name] : $default;
  }
  
  // Render the view of an action
  protected function render($action)
  {    
    ob_start();
    
    $view_path = $this->view_path($action);
    
    if (file_exists($view_path)) {
      include $view_path;
    }
    
    $output = ob_get_clean();

    echo $output;
  }

  protected final function base_url($sulfix = '')
  {
    return APP_BASE_URL . $sulfix;
  }

  protected function site_url($sulfix = '')
  {
    return $this->base_url($sulfix);
  }

  protected function public_url($sulfix = '')
  {
    return $this->base_url('public/' . $sulfix);
  }

  protected function get($propertie, $default = null)
  {
    return isset($this->_data[$propertie]) ? $this->_data[$propertie] : $default;
  }
  
  public function __get($propertie)
  {
    return $this->get($propertie);
  }
  
  public function __set($propertie, $value)
  {
    $this->_data[$propertie] = $value;
  }
}
