<?php

class Uri
{
  public static function get_querystring() {
    if (isset($_SERVER['ORIG_PATH_INFO'])) {
      return trim($_SERVER['ORIG_PATH_INFO'], '/');
    }
    
    $script = $_SERVER['SCRIPT_NAME'];
    $request = $_SERVER['PHP_SELF'];
    
    $querystring = substr($request, strlen($script));

    return trim($querystring, '/');
  }
}
