<?php

class TableDescriptor
{
  private static $instance;
  protected $tables;

  private function __construct()
  {
    $this->tables = array();
  }

  public static function get_instance()
  {
    if (!self::$instance) {
      self::$instance = new TableDescriptor();
    }

    return self::$instance;
  }

  public function __get($table)
  {
    if (!isset($this->tables[$table])) {
      $this->tables[$table] = DbCommand::table_fields($table);
    }

    return $this->tables[$table];
  }
}
