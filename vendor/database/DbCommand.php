<?php

class DbCommand
{
  private static $DB_HOST = "127.0.0.1";
  private static $DB_USER = "root";
  private static $DB_PASSWORD = "";
  private static $DB_DATABASE = "";

  protected static $connection = null;

  public static function configure($host, $user, $password, $database)
  {
    self::$DB_HOST = $host;
    self::$DB_USER = $user;
    self::$DB_PASSWORD = $password;
    self::$DB_DATABASE = $database;
  }

  protected static function connect()
  {
    if (self::$connection !== null) {
      return;
    }

    self::$connection = @mysqli_connect(self::$DB_HOST, self::$DB_USER, self::$DB_PASSWORD, self::$DB_DATABASE);

    if (!self::$connection) {
      throw new DbConnectionException("Unable to connect to database");
    }
  }

  protected static function check_connection()
  {
    if (self::$connection === null) {
      self::connect();
    }
  }

  protected static function query($sql)
  {
    self::check_connection();

    $result = @mysqli_query(self::$connection, $sql);

    if (!$result) {
      throw new InvalidQueryException("Error executing query: " . mysqli_error(self::$connection) . " at " . $sql);
    }

    return $result;
  }

  public static function execute($sql)
  {
    self::query($sql);

    return mysqli_affected_rows(self::$connection);
  }

  public static function row($sql)
  {
    $result = self::query($sql);

    return mysqli_fetch_array($result, MYSQLI_ASSOC);
  }

  public static function all($sql)
  {
    $result = self::query($sql);
    $data = array();

    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
      $data[] = $row;
    }

    return $data;
  }

  public static function table_fields($table)
  {
    $data = self::all("DESCRIBE `$table`");
    $fields = array();

    foreach ($data as $field) {
      $fields[] = $field['Field'];
    }

    return $fields;
  }

  public static function real_escape_string($string)
  {
    return mysqli_real_escape_string(self::$connection, $string);
  }
}

class DbConnectionException extends Exception {}
class InvalidQueryException extends Exception {}
