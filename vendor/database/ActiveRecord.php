<?php

require_once dirname(__FILE__) . '/DbCommand.php';
require_once dirname(__FILE__) . '/ModelCache.php';
require_once dirname(__FILE__) . '/TableDescriptor.php';
require_once dirname(__FILE__) . '/Validators.php';

abstract class ActiveRecord
{
  private $_exists;
  protected $_attributes;
  private $_validators;
  private $_errors;
  
  public function __construct($params = array())
  {
    $this->_exists = false;
    $this->_attributes = array();
    $this->_validators = array();
    $this->_errors = array();
    
    $this->initialize_fields();
    $this->fill($params);
    
    $this->setup();
  }
  
  // Get a shared instace of current model
  public static function model($model_name)
  {
    $model_name = ucfirst($model_name);
    $modelcache = ModelCache::get_instance();
    
    return $modelcache->$model_name;
  }
  
  public function setup() {}
  public function table() {}
  
  // Fill object with attributes with a given data array using key/value scheme
  public function fill($data)
  {
    foreach ($data as $key => $value) {
      $this->$key = $value;
    }
  }

  public function primary_key()
  {
    return 'id';
  }

  public function primary_key_value()
  {
    $pk = $this->primary_key();
    
    return $this->read_attribute($pk);
  }

  // Initialize instance attributes
  protected function initialize_fields()
  {
    $autospecial_fields = array(
      'created_at' => 'datetime',
      'updated_at' => 'datetime'
    );
    
    $fields = $this->fields();
    
    foreach ($fields as $field) {
      $this->_attributes[$field] = null;
    }
  }
  
  // Verify if the record exists at database
  public function exists()
  {
    return $this->_exists;
  }
  
  // Find records at database
  public function find($what = 'all', $options = array())
  {
    $options = array_merge(array(
      'conditions' => '',
      'order'      => $this->primary_key() . ' ASC',
      'select'     => '*',
      'from'       => '`' . $this->table() . '`'
    ), $options);
    
    switch ($what) {
      case 'all':
        return $this->find_every($options);
        break;
      
      default:
        return $this->find_from_ids($what, $options);
        break;
    }
  }
  
  // Wrapper for find with all as first argument
  public function all()
  {
    $args = func_get_args();
    array_unshift($args, 'all');
    
    return call_user_func_array(array($this, 'find'), $args);
  }
  
  // Save object into database, if the object exists, the instance is only updated at database
  public function save()
  {
    $this->create_or_update();
  }
  
  // Delete current register from database
  public function destroy()
  {
    //check if record exists before delete
    if (!$this->exists()) {
      return false;
    }
    
    $pk     = $this->primary_key();
    $pk_val = $this->sanitize($this->$pk);
    $table  = $this->table();
    
    $sql = "DELETE FROM `$table` WHERE `$pk` = '$pk_val'";
    
    DbCommand::execute($sql);
    
    $this->_exists = false;
    
    return true;
  }

  //Map result data into object
  protected function map_object($data)
  {
    $class = get_class($this);
    $object = new $class();
    
    foreach ($data as $key => $value) {
      $object->write_attribute($key, $value);
    }
    
    return $object;
  }
  
  private function construct_finder_sql($options)
  {
    $sql  = "SELECT {$options['select']} ";
    $sql .= "FROM {$options['from']} ";

    $this->add_conditions($sql, $options['conditions']);
    $this->add_order($sql, $options['order']);

    return $sql;
  }
  
  private function add_conditions(&$sql, $conditions)
  {
    $nest = array();
    
    if ($conditions) {
      $nest[] = $this->build_conditions($conditions);
    }
    
    if (count($nest) > 0) {
      $sql .= 'WHERE (' . implode(') AND (', $nest) . ') ';
    }
  }
  
  private function build_conditions($conditions)
  {
    $sql = '';
    
    if (is_array($conditions)) {
      if (array_keys($conditions) === range(0, count($conditions) - 1)) {
        $query = array_shift($conditions);
        
        for($i = 0; $i < strlen($query); $i++) {
          if ($query[$i] == '?') {
            if (count($conditions) == 0) {
              throw new QueryMismatchParamsException('The number of question marks is more than provided params');
            }
            
            $sql .= $this->prepare_for_value(array_shift($conditions));
          } else {
            $sql .= $query[$i];
          }
        }
      } else {
        $factors = array();
        
        foreach ($conditions as $key => $value) {
          $matches = array();
          
          if (preg_match("/([a-z_].*?)\s*((?:[><!=\s]|LIKE|IS|NOT)+)/i", $key, $matches)) {
            $key  = $matches[1];
            $op   = strtoupper($matches[2]);
          } else {
            if ($value === null) {
              $op = 'IS';
            } elseif (is_array($value)) {
              $op = 'IN';
            } else {
              $op = "=";
            }
          }
          
          $value = $this->prepare_for_value($value);
          
          $factors[] = "`$key` $op $value";
        }
        
        $sql .= implode(" AND ", $factors);
      }
    } else {
      $sql .= $conditions;
    }
    
    return $sql;
  }
  
  private function add_order(&$sql, $order)
  {
    if ($order) {
      $sql .= "ORDER BY $order";
    }
  }
  
  private function find_every($options)
  {
    return $this->find_by_sql($this->construct_finder_sql($options));
  }

  private function find_initial($options)
  {
    $options['limit'] = 1;
    
    $data = $this->find_every($options);
    
    return count($data) > 0 ? $data[0] : null;
  }
  
  private function find_from_ids($ids, $options)
  {
    $pk = $this->primary_key();
    
    if (is_array($ids)) {
      $options['conditions'] = "`$pk` in ('" . implode("','", $this->sanitize_array($ids)) . "')";
    } else {
      $id = $this->sanitize($ids);
      $options['conditions'] = "`$pk` = '$id'";
    }
    
    return is_array($ids) ? $this->find_every($options) : $this->find_initial($options);
  }

  public function find_by_sql($sql)
  {
    $data = DbCommand::all($sql);
    $data = array_map(array($this, 'map_object'), $data);

    foreach ($data as $model) {
      $model->_exists = true;
    }

    return $data;
  }
  
  private function create_or_update()
  { 
    if ($this->exists()) {
      $this->validate_ex();
      $this->update();
    } else {
      $this->validate_ex();
      $this->create();
    }
  }
  
  private function create()
  {
    $this->write_magic_time('created_at');
    $this->write_magic_time('updated_at');
    
    $pk = $this->primary_key();
    $table = $this->table();
    $fields = $this->map_real_fields();
    
    $sql_fields = implode("`,`", array_keys($fields));
    $sql_values = implode(",", array_map(array($this, 'prepare_for_value'), $fields));
    
    $sql = "INSERT INTO `$table` (`$sql_fields`) VALUES ($sql_values);";
    
    DbCommand::execute($sql);

    $this->_exists = true;
  }
  
  private function update()
  {
    $this->write_magic_time('updated_at');
    
    $pk = $this->primary_key();
    $pk_value = $this->sanitize($this->$pk);
    $table = $this->table();
    $fields = $this->map_real_fields();
    
    $sql_set = array();
    
    foreach ($fields as $key => $value) {
      $sql_set[] = "`$key` = " . $this->prepare_for_value($value);
    }
    
    $sql_set = implode(",", $sql_set);
    
    $sql = "UPDATE `$table` SET $sql_set WHERE `$pk` = '$pk_value';";
    
    DbCommand::execute($sql);
  }
  
  private function fields()
  {
    $descriptor = TableDescriptor::get_instance();
    $table = $this->table();
    
    return $descriptor->$table;
  }
  
  private function map_real_fields()
  {
    $pk = $this->primary_key();
    $data = array();
    $fields = $this->fields();
    
    foreach ($fields as $field) {
      if ($field != $pk) {
        $data[$field] = isset($this->_attributes[$field]) ? $this->_attributes[$field] : null;
      }
    }
    
    return $data;
  }
  
  private function map_real_fields_sanitized()
  {
    return $this->sanitize_array($this->map_real_fields());
  }
  
  private function sanitize($data)
  {
    if ($data === null) {
      return 'NULL';
    } elseif (is_array($data)) {
      return '(\'' . implode('\', \'', $this->sanitize_array($data)) . '\')';
    }
    
    return DbCommand::real_escape_string($data);
  }
  
  private function sanitize_array($data)
  {
    return array_map(array($this, "sanitize"), $data);
  }
  
  private function prepare_for_value($value)
  {
    $sanitized = $this->sanitize($value);
    
    if (is_string($value)) {
      return "'$sanitized'";
    } else {
      return $sanitized;
    }
  }
  
  public function read_all_attributes()
  {
    return $this->_attributes;
  }
  
  public function read_attribute($attribute)
  {
    return isset($this->_attributes[$attribute]) ? $this->_attributes[$attribute] : null;
  }
  
  public function write_attribute($attribute, $value)
  {
    $this->_attributes[$attribute] = $value;
  }
  
  private function write_magic_time($field)
  {
    $fields = $this->fields();
    
    if (in_array($field, $fields)) {
      $date = date('Y-m-d H:i:s');
      $this->write_attribute($field, $date);
    }
  }
  
  // Handles access to dynamic properties
  public function __get($attribute)
  {
    //check for method accessor
    if (method_exists($this, 'get_' . $attribute)) {
      return call_user_func(array($this, 'get_' . $attribute));
    }

    //check for id
    if ($attribute == 'id') {
      return $this->primary_key_value();
    }

    //get table attribute
    return $this->read_attribute($attribute);
  }
  
  // Handles access to write dynamic properties
  public function __set($attribute, $value)
  {
    //check for method accessor
    if (method_exists($this, 'set_' . $attribute)) {
      call_user_func(array($this, 'set_' . $attribute), $value);
    } else {
      //set attribute
      $this->write_attribute($attribute, $value);
    }
  }
  
  // Handles access to dynamic methods
  public function __call($name, $arguments)
  {
    //for use in preg matches
    $matches = array();
    
    //do a get
    if (preg_match('/^get_(.+)/', $name, $matches)) {
      $var_name = $matches[1];
      
      return $this->$var_name ? $this->$var_name : $arguments[0];
    }
    
    //try to catch validator assign
    if (substr($name, 0, 9) == 'validates') {
      return $this->register_validator($name, $arguments);
    }
    
    //send to model try to parse
    $this->call($name, $arguments);
  }
  
  public function call($name)
  {
    throw new InvalidMethodException("Method $name is not found in " . get_class($this));
  }
  
  // Validators
  private function register_validator($validator, $arguments)
  {
    array_unshift($arguments, $this);
    
    $this->_validators[] = array($validator, $arguments);
  }
  
  protected function validate_ex()
  {
    if (!$this->is_valid()) {
      throw new InvalidRecordException('This record has some invalid fields, please fix problems and try again');
    }
  }
  
  // Test if current object is valid
  public function is_valid()
  {
    $this->_errors = array();
    $valid = $this->validate();
    
    foreach ($this->_validators as $validator) {
      list($method, $arguments) = $validator;
      
      if (!call_user_func_array(array('Validators', $method), $arguments)) {
        $valid = false;
      }
    }
    
    return $valid;
  }
  
  // Inject one error at object
  public function add_error($field, $error)
  {
    $this->_errors[$field][] = $error;
  }
  
  // Check if a field contains errors
  public function field_has_errors($field)
  {
    return isset($this->_errors[$field]);
  }
  
  // Get a flatten array with all errors
  public function problems()
  {
    $flat = array();
    
    foreach ($this->_errors as $field_errors) {
      foreach ($field_errors as $error) {
        $flat[] = $error;
      }
    }
    
    return $flat;
  }

  public function validate()
  {
    return true;
  }
}

//Exceptions
class InvalidRecordException extends Exception {}
class QueryMismatchParamsException extends Exception {}
class InvalidMethodException extends Exception {}
