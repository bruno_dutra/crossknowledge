<?php

class Validators
{
  public static function validates_presence_of($object, $field, $err)
  {
    if (!$object->$field) {
      $object->add_error($field, $err);
      return false;
    }
    
    return true;
  }
}
