<?php

function app_autoloader($classname)
{
  //check if class already exists
  if (class_exists($classname)) {
    return true;
  }
  
  //check if is a core library
  $path = APP_SYSTEM_LIBRARIES_PATH . '/' . $classname . '.php';
  
  if (file_exists($path)) {
    require_once $path;
    
    if (class_exists($classname)) {
      return true;
    }
  }
  
  //check if the class is a controller
  if (substr($classname, -strlen('Controller')) == 'Controller') {
    $path = APP_CONTROLLERS_PATH;
    
    $bits = explode('_', $classname);
    
    while (count($bits) > 1) {
      $path .= '/' . strtolower(array_shift($bits));
    }
    
    $path .= '/' . substr(array_shift($bits), 0, -strlen('Controller')) . '.php';
    
    if (file_exists($path)) {
      require_once $path;
      
      if (class_exists($classname)) {
        return true;
      }
    }
  }
  
  //try to load model
  $path = APP_MODELS_PATH . '/' . $classname . '.php';
  
  if (file_exists($path)) {
    require_once $path;
    
    if (class_exists($classname)) {
      return true;
    }
  }
  
  //not found the class
  return false;
}

//register autoloader
spl_autoload_register('app_autoloader');
