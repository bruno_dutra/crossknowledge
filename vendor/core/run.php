<?php

//autoloader for classes
require_once APP_SYSTEM_CORE_PATH . '/autoloader.php';

//load router configuration
require_once APP_CONFIG_PATH . '/router.php';

//disable magic quotes
require_once APP_SYSTEM_CORE_PATH . '/magic_quotes.php';

//load database
require_once APP_SYSTEM_DATABASE_PATH . '/ActiveRecord.php';

//load configuration basics
$conf = include_once APP_CONFIG_PATH . '/config.php';

define('APP_BASE_URL', $conf->base_url);

DBCommand::configure($conf->host, $conf->user, $conf->password, $conf->database);

//load uri
$current_uri = Uri::get_querystring();

$ROUTER = new AppRouter();
$current_route = $ROUTER->match($current_uri);

//run!
Controller::run($current_route);
