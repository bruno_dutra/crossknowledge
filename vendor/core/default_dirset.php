<?php

//application directory definitions
define('APP_APP_PATH', APP_ROOT_PATH . '/app');
define('APP_CONFIG_PATH', APP_ROOT_PATH . '/config');
define('APP_PUBLIC_PATH', APP_ROOT_PATH . '/public');
define('APP_VENDOR_PATH', APP_ROOT_PATH . '/vendor');

define('APP_CONTROLLERS_PATH', APP_APP_PATH . '/controllers');
define('APP_MODELS_PATH', APP_APP_PATH . '/models');
define('APP_VIEWS_PATH', APP_APP_PATH . '/views');

//system directory definition
define('APP_SYSTEM_CORE_PATH', APP_SYSTEM_ROOT_PATH . '/core');
define('APP_SYSTEM_DATABASE_PATH', APP_SYSTEM_ROOT_PATH . '/database');
define('APP_SYSTEM_HELPERS_PATH', APP_SYSTEM_ROOT_PATH . '/helpers');
define('APP_SYSTEM_LIBRARIES_PATH', APP_SYSTEM_ROOT_PATH . '/libraries');
