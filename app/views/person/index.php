<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="">
  
  <title>CrossKnowledge</title>

  <base href="<?= $this->site_url() ?>">
  
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="css/style.css"  media="screen,projection"/>
</head>

<body>
  <nav class="grey darken-4">
    <div class="nav-wrapper container">
      <a class="brand-logo">CrossKnowledge</a>
    </div>
  </nav>

  <div class="container">
    <div class="section" id="loader">
      <div class="loader-content center-align">
        <div class="col s12">
          <p>Carregando...</p>
          <div class="progress grey darken-3">
            <div class="indeterminate grey"></div>
          </div>
        </div>
      </div>
    </div>

    <div class="section people hide">
      <div class="row">
        <h3>Cadastros</h3>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="pull-right">
            <a href="#" class="btn-new btn waves-effect waves-light right blue-grey darken-3"><i class="material-icons left">add</i> Novo</a>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col s12">
          <table class="responsive-table">
            <thead>
              <tr>
                <th data-field="name">Nome</th>
                <th data-field="surname">Sobrenome</th>
                <th data-field="address">Endereço</th>
                <th data-field="created">Crieado em</th>
                <th data-field="updated">Atualizado em</th>
                <th>Funções</th>
              </tr>
            </thead>

            <tbody id="content-people">
            </tbody>
          </table>   
        </div>
      </div>
    </div>

    <div id="modal-form" class="modal">
      <div class="modal-content">
        <h4>Adicionar cadastro</h4>

        <div class="row">
          <form class="col s12" action="<?= $this->site_url('person/save') ?>" method="post" id="form-register">
            <input type="hidden" name="id">

            <div class="row">
              <div class="input-field col s6">
                <input id="name" name="data[name]" type="text" class="validate">
                <label for="name">Nome</label>
              </div>
              <div class="input-field col s6">
                <input id="surname" name="data[surname]" type="text" class="validate">
                <label for="surname">Sobrenome</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <input id="address" name="data[address]" type="text" class="validate">
                <label for="address">Endereço</label>
              </div>
            </div>

            <button class="btn waves-effect waves-light blue-grey darken-3" type="submit">Salvar
              <i class="material-icons left">save</i>
            </button>
            
            <button class="btn btn-cancel waves-effect waves-light blue-grey">Cancelar
              <i class="material-icons left">cancel</i>
            </button>
          </form>
        </div>
      </div>
    </div>

    <div id="modal-confirm" class="modal">
      <div class="modal-content">
        <p>Remover esse item?</p>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">Não</a>
        <a href="#!" data-id="" class="btn-remove-action modal-action modal-close waves-effect waves-green btn-flat">Sim</a>
      </div>
    </div>
  </div>

  <footer class="page-footer blue-grey darken-3">
    <div class="footer-copyright valign-wrapper">
      <div class="container valign">
      Made by <a class="grey-text text-lighten-3" href="http://brunodutra.com.br" target="_blank">Bruno Dutra</a>
      </div>
    </div>
  </footer>
  <!-- scripts -->
  <script src="<?= $this->public_url('js/lib/jquery-3.2.0.min.js') ?>"></script>
  <script src="<?= $this->public_url('js/lib/materialize.min.js') ?>"></script>
  <script src="<?= $this->public_url('js/app/person.js') ?>"></script>
</body>
</html>