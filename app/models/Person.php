<?php

class Person extends ActiveRecord
{
  public function setup()
  {
    $this->validates_presence_of('name', 'Digite o nome');
    $this->validates_presence_of('surname', 'Digite o sobrenome');
    $this->validates_presence_of('address', 'Digite o endereço');
  }

  public function table()
  {
    return 'people';
  }

  public function get_created_at_format()
  {
    return date('d/m/Y H:i', strtotime($this->created_at));
  }

  public function get_updated_at_format()
  {
    return date('d/m/Y H:i', strtotime($this->updated_at));
  }
}
