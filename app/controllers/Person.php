<?php

class PersonController extends Controller
{
  public function refresh()
  {
    $people = ActiveRecord::model('Person')->all(array(
      'order'=> 'name asc'
    ));

    $message = !$people ? 'No result.' : '';

    $data = array();

    foreach ($people as $person) {
      $data[] = array_merge($person->read_all_attributes(), array(
        'created_at_format' => $person->created_at_format,
        'updated_at_format' => $person->updated_at_format
      ));
    }

    $this->returnJson(200, $message, $data);
  }

  public function edit()
  {
    $id = $this->param('id');
    $person = ActiveRecord::model('Person')->find($id);

    if ($person) {
      $person = $person->read_all_attributes();
    }

    $message = !$person ? 'No result.' : '';

    $this->returnJson(200, $message, $person);
  }
  
  public function save()
  {
    $id = $this->param('id');

    $person = $id === null ? new Person : ActiveRecord::model('Person')->find($id);
    
    if (isset($_POST['data'])) {
      $person->fill($_POST['data']);
      
      if ($person->is_valid()) {
        $person->save();

        $this->returnJson(200, 'Saved successfully.');
      } else {
        $this->returnJson(400, 'Bad request.', $person->problems());
      }
    }
  }
  
  public function remove()
  {
    $id = $this->param('id', '');
    $ids = explode(',', $id);
    
    $people = ActiveRecord::model('Person')->all(array('conditions' => array('id' => $ids)));
    
    foreach ($people as $person) {
      $person->destroy();
    }

    $this->returnJson(200, 'Deleted successfully.');
  }

  private function returnJson($status, $message = '', $data = array())
  {
    http_response_code($status);

    header('Content-Type: application/json');

    $json['status'] = $status;

    if ($message) {
      $json['message'] = $message;
    }

    if ($data) {
      $json['data'] = $data;
    }

    echo json_encode($json);
  }
}
