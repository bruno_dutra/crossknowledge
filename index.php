<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set('America/Sao_Paulo');

define('APP_ROOT_PATH', dirname(__FILE__));
define('APP_SYSTEM_ROOT_PATH', APP_ROOT_PATH . '/vendor');

require_once APP_SYSTEM_ROOT_PATH . '/core/default_dirset.php';
require_once APP_SYSTEM_CORE_PATH . '/run.php';
