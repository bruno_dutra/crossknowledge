(function () {
  'use strict';

  var _base_url = $('base').attr('href');

  var _refresh_people = function(){
    $('#loader').fadeIn(300);

    $.getJSON(_base_url + 'person/refresh', function(resp){
      $('#loader').fadeOut(300, function(){
        var people = [];

        $.each(resp.data, function(key, person) {
          people.push('<tr><td>' + person.name + '</td><td>' + person.surname + '</td><td>' + person.address + '</td><td>' + person.created_at_format + '</td><td>' + person.updated_at_format + '</td><td><a href="#" class="btn-edit btn-floating btn-small waves-effect waves-light blue-grey darken-3" data-id="' + person.id + '"><i class="small material-icons">mode_edit</i>Editar</a><a href="#" class="btn-remove btn-floating btn-small waves-effect waves-light red" data-id="' + person.id + '"><i class="small material-icons">delete_forever</i>Remover</a></td></tr>');
        });

        $('#content-people').html(people.join(''));
        $('.section.people').removeClass('hide');

        $('.btn-edit').on('click', function(e){
          e.preventDefault();

          $('.modal-content h4').text('Editar cadastro');

          var id = $(this).attr('data-id');

          $.getJSON(_base_url + 'person/edit/' + id, function(resp){
            var person = resp.data;
            
            if (person) {
              $('#form-register').find('input[name="id"]').val(person.id);
              $('#form-register').find('input[name="data[name]"]').val(person.name);
              $('#form-register').find('input[name="data[surname]"]').val(person.surname);
              $('#form-register').find('input[name="data[address]"]').val(person.address);

              Materialize.updateTextFields();
              $('#modal-form').modal('open');
            }
          });
        });

        $('.btn-remove').on('click', function(e){
          e.preventDefault();

          $('.btn-remove-action').attr('data-id', $(this).attr('data-id'));
          $('#modal-confirm').modal('open');
        });
      });
    });
  };

  $('.btn-remove-action').on('click', function(e){
    e.preventDefault();

    $.getJSON(_base_url + 'person/remove', {id: $(this).attr('data-id')}, function(data){
      _refresh_people();
    });
  });

  $('#form-register').submit(function(e){
    e.preventDefault();

    var form = $(this);
    var error = false;

    $('.msg-sucess, .msg-error').addClass('hide');
    $('input.invalid').removeClass('invalid');

    form.find('input:text').each(function(){
      if (!$(this).val()) {
        $(this).addClass('invalid');
        error = true;
      }
    });

    if (error) {
      $('.msg-error').removeClass('hide');
    } else {
      $.ajax({
        type: 'post',
        url: _base_url + 'person/save',
        data: form.serialize(),
        success: function() {
          form.get(0).reset();
          $('.msg-success').removeClass('hide');
          $('#modal-form').modal('close');

          _refresh_people();
        }
      });
    }
  });

  _refresh_people();

  $('#modal-form, #modal-confirm').modal();

  $('.btn-new').on('click', function(e){
    e.preventDefault();

    $('.modal-content h4').text('Adicionar cadastro');
    $('#modal-form').modal('open');
  });

  $('.btn-cancel').on('click', function(e){
    e.preventDefault();

    $('#form-register').get(0).reset();
    Materialize.updateTextFields();

    $('#modal-form').modal('close');
  });
})();