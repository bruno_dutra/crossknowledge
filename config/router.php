<?php

class AppRouter extends Router
{
  public function setup()
  {
    $this->map_connect(':controller/:action/:id');
    $this->map_connect(':controller/:action');
    $this->map_connect(':action', array("controller" => "person"));
    $this->map_connect('', array("controller" => "person"));
  }
}
