<?php

$conf = new Configuration();

$conf->base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/crossknowledge/';

$conf->host = 'localhost';
$conf->user = 'root';
$conf->password = '';
$conf->database = 'crossknowledge_test';

return $conf;
