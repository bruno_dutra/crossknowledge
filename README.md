# Teste CrossKnowledge

## Orientações

1. Importar o script de criação do banco de dados necessário para esta aplicação. Caminho `/db/crossknowledge_test.sql`;
2. Altere os dados de acesso ao banco de dados em `/config/config.php`;

Pronto! :)

## Créditos

* Criado por [bruno_dutra](https://bitbucket.org/bruno_dutra/).